# alpine_ops

## Why?

This is my portable DevOps env.

## Where?

"No place like [localhost](http://127.0.0.1)"

## How?

Docker:

```bash
# (with) docker-compose
docker-compose up
```
