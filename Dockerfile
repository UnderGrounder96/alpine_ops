FROM alpine

ENV USER="app" LANG="en_US.UTF-8"

RUN --mount=type=cache,target=/etc/apk/cache \
  --mount=source=scripts/deps.sh,target=/tmp/deps.sh \
  sh /tmp/deps.sh

RUN --mount=source=scripts/root.sh,target=/tmp/root.sh \
  sh /tmp/root.sh

VOLUME [ "/etc/apk/cache" ]

COPY conf/tmux.conf /etc/
COPY conf/sh_aliases.sh /etc/profile.d/
COPY conf/.gitconfig /tmp/.gitconfig

USER ${USER}

WORKDIR /mnt/wksc

RUN --mount=source=scripts/user.sh,target=/tmp/user.sh \
  sh /tmp/user.sh
