# User specific aliases and functions
alias diffs="sdiff -s" # sdiff -s <(cat -n ${1}) <(cat -n ${2})
alias ggrep="grep -nir --exclude=*{.md,.txt,*lock*} --exclude-dir={.git,node_modules,*cache*,log}"
alias py3="python3"

# nvim
alias vim="nvim"

# git
alias g="git"
alias gc="g config"
alias gcl="g config --list"
alias gcg="g config --global"

# docker
alias d="docker"
alias dsp="d system prune -af --volumes"

alias dc="docker compose"
alias dcd="dc down"
alias dcu="dc up -d --build"
alias dcl="dc logs -tf"
alias dcdu="dcd && dcu"
alias dcul="dcu && dcl"
alias dcdul="dcd && dcu && dcl"
