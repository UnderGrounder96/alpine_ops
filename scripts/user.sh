#!/usr/bin/env sh

set -euo pipefail

NVCONFIGS="https://gitlab.com/UnderGrounder96/nvim-configs.git"

ln -s /mnt/local ~/.local
ln -s /mnt/cache ~/.cache
ln -s /mnt/config ~/.config
ln -s /tmp/.gitconfig ~/.gitconfig

git clone --depth 1 ${NVCONFIGS} ~/.config/nvim

# FIXME: appears to be stuck!?
# nvim --headless "+q"
