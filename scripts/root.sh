#!/usr/bin/env sh

set -euo pipefail

adduser -DG root ${USER}

for i in "wksc/dev" "wksc/ops" "wksc/logs" "config" "cache" "local" "files"; do
  mkdir -p /mnt/${i}
done

chown -R ${USER} /mnt
