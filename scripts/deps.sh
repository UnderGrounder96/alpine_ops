#!/usr/bin/env sh

set -euo pipefail

# needed for libstdc
LD_LIBRARY_PATH=/lib

# deps
apk add libstdc++ docker-cli docker-cli-compose zig go git nodejs npm curl \
  openssh-client ansible-core neovim ripgrep tmux py3-pip
